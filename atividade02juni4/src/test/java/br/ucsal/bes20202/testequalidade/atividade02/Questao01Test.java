package br.ucsal.bes20202.testequalidade.atividade02;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class Questao01Test {

	@Parameters(name = "{index} - testarVerificarPrimo({0})->{1}")
	public static Collection<Object[]> carregarCasosTeste() {
		return Arrays.asList(new Object[][] { { 3, true }, { 4, false }, { 17, true } });
	}

	@Parameter(0)
	public Integer n;

	@Parameter(1)
	public Boolean saidaEsperada;

	@Test
	public void testar() {
		Boolean saidaAtual = Questao01.verificarPrimo(n);
		assertEquals(saidaEsperada, saidaAtual);
	}

}
