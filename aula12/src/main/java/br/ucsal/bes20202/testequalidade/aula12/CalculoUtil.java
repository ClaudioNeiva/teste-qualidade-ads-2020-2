package br.ucsal.bes20202.testequalidade.aula12;

public class CalculoUtil {

	public static Long calcularFatorial(Integer n) {
		if (n == 0) {
			return 1L;
		}
		return n * calcularFatorial(n - 1);
	}

}
