package br.ucsal.bes20202.testequalidade.aula14;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PesquisaAtivo2Steps {

	private WebDriver driver;

	public PesquisaAtivo2Steps(WebDriver driver) {
		this.driver = driver;
	}

	@Given("estou no site $url")
	public void abrirSite(String url) throws InterruptedException {
		driver.get(url);
	}

	@When("informo $valorPesquisa no campo de pesquisa")
	public void informarValorPesquisa(String valorPesquisa) {
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys(valorPesquisa);
	}

	@When("teclo ENTER")
	public void teclarEnter() {
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys(Keys.ENTER);
	}

	@Then("é exibido na página o texto $conteudoEsperado")
	public void verificarConteudoPagina(String conteudoEsperado) throws InterruptedException {
		Thread.sleep(5000);
		String conteudo = driver.getPageSource();
		Assert.assertTrue(conteudo.contains(conteudoEsperado));
	}

}
